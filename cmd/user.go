package cmd

import "github.com/spf13/cobra"

var userCmd = &cobra.Command{
	Use:   "user",
	Short: "interact with Jira users",
}

func init() {
	userCmd.AddCommand(userGetCmd)
}
