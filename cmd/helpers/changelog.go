package helpers

import (
	"bytes"
	"fmt"
	"github.com/andreyvit/diff"
	"strings"
)

type Change struct {
	old, new         string
	serialise, print bool
}

type ChangeLog struct {
	changes map[string]*Change
}

func NewChangeLog() *ChangeLog {
	return &ChangeLog{make(map[string]*Change, 0)}
}

func (cl *ChangeLog) AddChange(key, old, new string, serialise, print bool) {
	cl.changes[key] = &Change{old, new, serialise, print}
}

func (cl *ChangeLog) ToMap() map[string]interface{} {
	out := make(map[string]interface{}, len(cl.changes))
	for k, v := range cl.changes {
		if !v.serialise {
			continue
		}
		out[k] = v.new
	}
	return out
}

func (cl *ChangeLog) String() string {
	var buffer bytes.Buffer

	for k, v := range cl.changes {
		if !v.print { continue }
		buffer.WriteString(fmt.Sprintf("Change: [%s]\n%s\n", strings.Title(k), diff.LineDiff(v.old, v.new)))
	}

	return buffer.String()
}

func (cl *ChangeLog) IsEmpty() bool {
	return len(cl.changes) == 0
}
