package errors

import "github.com/pkg/errors"

const errorPrefix = "cli_error"

type CliError struct {
	err error
}

func NewCliError(err error) *CliError {
	return &CliError{err}
}

func (ce *CliError) Error() string {
	return errors.Wrap(ce.err, errorPrefix).Error()
}
