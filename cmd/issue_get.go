package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Retrieves multiple issues based on the provided identifiers",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the issue identifier to retrieve details from")
		}
		for _, issueID := range args {
			issue, err := jiraController.GetIssue(issueID, nil)
			if err != nil {
				return nil
			}
			if expand {
				fmt.Printf("%+v\n%+v\n", issue, issue.Fields)
			} else {
				fmt.Printf("%v\n", issue.ID)
			}

		}
		return nil
	},
}
