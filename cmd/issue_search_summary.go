package cmd

import (
	"bytes"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"os"
	"strings"
)

var (
	status string
)

var searchBySummaryCmd = &cobra.Command{
	Use:   "summary",
	Short: "Searches for a Jira ticket based on the supplied summary content (exit status 1 if no issue was found)",
	Example: `jira-cli issue search summary -p <project_name> -t <issue_type> -s <issue_status> -e <custom_field>=<value> "<summary_content>"`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify a Summary pattern")
		}
		// Process query
		jql := fmt.Sprintf(`Summary ~ "%s"`, args[0])
		processFlags(&jql)

		// API call
		issues, err := jiraController.SearchIssues(jql, composeSearchOptions(), maxOccurrences)
		if err != nil {
			return errors.NewCliError(err)
		}
		// 1 -> Indicate that no issue was found
		if len(issues) == 0 {
			os.Exit(1)
		}

		for _, i := range issues {
			fmt.Println(i.Key)
		}
		return nil
	},
}

func processFlags(jql *string) {
	var buf bytes.Buffer
	if len(strings.TrimSpace(project)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND project = "%s" `, project))
	}
	if len(strings.TrimSpace(issueType)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND type = "%s" `, issueType))
	}
	if len(strings.TrimSpace(status)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND status = "%s" `, status))
	}
	if len(customFields) != 0 {
		for k, v := range customFields {
			buf.WriteString(fmt.Sprintf(` AND %s = "%s" `, k, v))
		}
	}

	if buf.Len() > 0 {
		*jql += buf.String()
	}
}

func init() {
	searchBySummaryCmd.Flags().StringVarP(&project, "project", "p", "", "the issue project (optional)")
	searchBySummaryCmd.Flags().StringVarP(&issueType, "type", "t", "", "the issue type (optional)")
	searchBySummaryCmd.Flags().StringVarP(&status, "status", "s", "", "the issue status (optional)")
	searchBySummaryCmd.Flags().StringToStringVarP(&customFields, "extra", "e", nil, `additional custom fields to be used to find issues (optional) (e.g. '-e "labels=X" -e "component=Y"'`)
}
