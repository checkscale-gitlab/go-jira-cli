package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira-cli/cmd/errors"
)

var userGetCmd = &cobra.Command{
	Use:   "get",
	Short: "Find a Jira user based on the provided username",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify a username")
		}
		user, err := jiraController.GetUser(args[0])
		if err != nil {
			return errors.NewCliError(err)
		}

		if expand {
			fmt.Printf("%+v\n", user)
		} else {
			fmt.Println(user.Key)
		}
		return nil
	},
}
