package internal

import (
	"bytes"
	"fmt"
	"gitlab.com/pcanilho/go-jira"
)

func (j *jiraController) GetBoard(boardID string) (*jira.Board, error) {
	boardList, _, err := j.client.Board.GetAllBoards(&jira.BoardListOptions{Name: boardID})
	if err != nil {
		return nil, err
	}
	// Abort if the search returned more than 1 board
	if len(boardList.Values) > 1 {
		var buffer bytes.Buffer
		for i, b := range boardList.Values {
			buffer.WriteString(b.Name)
			if i < len(boardList.Values) - 1 {
				buffer.WriteRune(',')
			}
		}
		return nil, fmt.Errorf("the supplied board identifier returned [%v] results instead of a unique board," +
			" please refine your search", buffer.String())
	}
	return &boardList.Values[0], nil
}

func (j *jiraController) GetBoardIssues(boardID string) ([]jira.Issue, error) {
	board, err := j.GetBoard(boardID)
	if err != nil {
		return nil, err
	}
	filter, _, _ := j.client.Filter.Get(board.FilterID)
	issues, _, err := j.client.Issue.Search(filter.Jql, nil)
	return issues, err
}