# <img alt="go-jira-cli logo" src="https://gitlab.com/uploads/-/system/project/avatar/28021464/827f56482d636c94f19b20f4cef561e3a668b280.png" height="100" width="100"> go-jira-cli (`jira-cli`)

[![View - Documentation](https://img.shields.io/badge/View-Documentation-blue?style=for-the-badge)](https://go-jira-cli.canilho.net/)

## Features

* Authentication (via the standard Jira SDK available [here](https://github.com/andygrunwald/go-jira))
* CRUD (`Create`, `Read`, `Update` & `Delete`) operations on issues via Key or Identifier.
* Search tickets by supplying a `JQL` query. Searches can also be asserted to return only a specified number of rows as 
to ascertain that piped commands work as expected.
* Searches can also be done via the content of the issue `Summary`. Additional filters are also supplied to aid in the filtering of the output such as `project`, `type`, `status` & custom field by using the following pattern `<custom_field_name>=<custom_field_value>`.
* Full support for piped commands (`xargs`) via the `-f` or `--force` global flags.
* Full support for custom fields during `creation`, `search` or `update` of issues.

### Note
This project does not wrap the entire functionality of the Jira API. It is being extended on a per-need basis. 

## Artefacts

| **OS**        | **ARCH**      | **Version**       | **Link** | **SHA512** |
| ------------- |:-------------:| :-------------:   | -------------:| -------------:|
| `Darwin`      | `x64`         | `v0.0.4`          | [download](https://gitlab.com/pcanilho/go-jira-cli/-/jobs/artifacts/v0.0.4/raw/binaries/jira-cli.v0.0.4_darwin_amd64?job=prepare_job) | `afd2b459913e01d84a0eda03ae0a0db322ede22ef68371ce4614c04b6ce213ebc88525351bf5d0a9ab979055a6dbaa8106637c008482dbf82b38739ca95f1a52` |
| `Linux`       | `x64`         | `v0.0.4`          | [download](https://gitlab.com/pcanilho/go-jira-cli/-/jobs/artifacts/v0.0.4/raw/binaries/jira-cli.v0.0.4_linux_amd64?job=prepare_job) | `0bd07c6869f7af5faf33172df31c3de7f1b094361ffcd821ae918ab318fdb32db28a499c459285f244ef2f16000c5367741de2e9478ce4c4b0c367ab6d61ddea` |
| `Windows`     | `x64`         | `v0.0.4`          | [download](https://gitlab.com/pcanilho/go-jira-cli/-/jobs/artifacts/v0.0.4/raw/binaries/jira-cli.v0.0.4_windows_amd64.exe?job=prepare_job) | `49e610dc843e792f3c3eaeea5d68f470288ce4bfb4d15b475c48b7eedf3c1663ff865d593f98625fac5dd9bf5f0a4b177f9d0f08c514a958e2e7f36b7e72bb8e` |

## Requirements

* Go >= `1.14` (dev-only)
* Jira `v6.3.4` to `v7.1.2`.
* `direnv` (optional) (dev-only)

## Examples

### Searching for tickets keys using a `JQL` query

Via `cli`:

```bash
$ jira-cli issue search 'created >= -4w ORDER BY created DESC' 
```

Via `docker`:

```bash
$ docker run -it --rm gitlab.com/pcanilho/go-jira-cli:<version> issue search 'created >= -4w ORDER BY created DESC' 
```

Via `library`:

```go
jql := "created >= -4w ORDER BY created DESC"
if issues, err := jiraController.SearchIssues(jql, nil, 0); err != nil {
	return err
} else {
	for _, issue := range issues {
	    fmt.Println(issue.Key)	
    }   
}
```

## Installation

Via `go get`:

```bash
$ go get gitlab.com/pcanilho/go-jira-cli
```

Via `docker`:
```bash
$ docker pull gitlab.com/pcanilho/go-jira-cli:<version>
```

Via `stand-alone` package (binary):
```bash
curl -L -o jira_cli "https://gitlab.com/pcanilho/go-jira-cli/-/jobs/artifacts/<version>/raw/binaries/jira-cli.<version>_<os>_<arch>?job=prepare_job"
```

* Example for the version `0.0.4`, `darwin` & 64-bit:

```bash
curl -L -o jira_cli "https://gitlab.com/pcanilho/go-jira-cli/-/jobs/artifacts/v0.0.4/raw/binaries/jira-cli.v0.0.4_darwin_amd64?job=prepare_job"
```

Using it as a library:

```go
package main

import (
    jira_cli "gitlab.com/pcanilho/go-jira-cli"
)
...
```

## Configuration

The authentication secrets used to interact with the Jira API can be provided either via arguments to the applications,
as seen below, or via environment variables. For the latter approach, I fully recommend the usage of `direnv` allied
with a `.env` file so that secrets can be loaded automatically.

* Via arguments:
```shell
Global Flags:
      --jira.password string   [JIRA_PASSWORD] the password used to authenticate with Jira
      --jira.url string        [JIRA_URL] the Jira instance URL
      --jira.username string   [JIRA_USERNAME] the username used to authenticate with Jira
```

* Via `direnv` + `.env` file:

1. Copy the sample environment file:

```shell
$ cp env.sample .env
``` 

2. Overwrite the values with the ones appropriate to your use-case.
3. Allow `direnv` to extract those values whenever you visit the folder that has the previously created `.env` file:

```shell
$ direnv allow
direnv: export +JIRA_PASSWORD +JIRA_URL +JIRA_USERNAME
```

## Building

For convenience’s sake, I have created a `Makefile` that can be used, as shown below, to create releases of this project
for a target operating system and architecture:

* Build for `Linux` + `amd64`:

```shell
$ make build
```

* Build for `Windows` + `x86`:

```shell
$ make build TARGET_OS=windows TARGET_ARCH=386
```

* Build with `cgo`:

```shell
$ make build CGO_ENABLED=1
```

* Build for all operating-systems + `x64`:

```shell
$ make build-all
```

* Build docker container:

```shell
$ make build-docker
```

## Interfaces

Internally, I created the `Controller` interface to more easily declare which features are available via this project. 
It can be used as a starting point for any extensions. However, I suggest the usage/creation of other interfaces that translate additional 
functionality as to mitigate upstream conflicts.

`internal/controller.go`:
```go
type Controller interface {
	IssueController
	...
}

type IssueController interface {
	CreateIssue(*IssueCreationOptions) (*jira.Issue, error)
	CloneIssue(interface{}) (*jira.Issue, error)
	SearchIssues(string, *jira.SearchOptions, int) ([]jira.Issue, error)
	GetIssue(string, *jira.GetQueryOptions) (*jira.Issue, error)
	UpdateIssue(interface{}, map[string]interface{}) (*jira.Issue, error)
	AddCommentToIssue(interface{}, string) (*jira.Comment, error)
	UploadAttachmentsToIssue(interface{}, ...*IssueAttachment) error
	DeleteIssue(interface{}) error
}
```
