FROM golang:latest as builder
LABEL version=0.0.4 author=paulo@canilho.net support=jira-cli_support@canilho.net
# Build
WORKDIR /src
ADD . .
RUN make build-trim

FROM debian:10-slim
WORKDIR /app
COPY --from=builder /src/binaries/jira-cli.linux .
ENV JIRA_URL=${JIRA_URL}
ENV JIRA_PASSWORD=${JIRA_PASSWORD}
ENV JIRA_USERNAME=${JIRA_USERNAME}
CMD ["/app/jira-cli.linux"]
